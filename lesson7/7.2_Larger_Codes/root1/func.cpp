#include <cmath>
using namespace std;

double quadratic_root1(double a, double b, double c) {
    // Return the larger root of a quadratic equation.

    double root1;

    root1 = -b + sqrt(pow(b, 2) - 4.0 * a * c);
    root1 = root1 / (2.0 * a);
    return root1;
}
