// Example showing how to use data structures
#include <iostream>
#include <string>

using namespace std;

// We use the following construction to create a structure type named
// Atom, containing several different types of variables.
struct Atom {
    int atomic_number;
    double mass_number;
    double radius_pm;
    string symbol;
} ;
// Note we need to finish the definition with a ';'.

// This function will output the information stored in the structure.
// variables contained in the structure are accessed using the
// structure_name.variable_name construction.
void output_atom_info(Atom a) {
    cout << a.symbol << endl;
    cout << "Atomic number: " << a.atomic_number << endl;
    cout << "Mass number: " << a.mass_number << endl;
    cout << "Atomic radius (pm): " << a.radius_pm << endl;
}

int main() {
    // Initialize two objects of the Atom type.
    Atom hydrogen, lithium;

    // We can set the variables contained in each directly.
    hydrogen.atomic_number = 1;
    hydrogen.mass_number = 1.008;
    hydrogen.symbol = "H";
    hydrogen.radius_pm =  25;

    lithium.atomic_number = 3;
    lithium.mass_number = 6.941;
    lithium.symbol = "Li";
    lithium.radius_pm = 145;

    output_atom_info(hydrogen);
    cout << endl;
    output_atom_info(lithium);

    return 0;
}
