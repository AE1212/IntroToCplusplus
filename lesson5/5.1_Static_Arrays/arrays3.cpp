// Example showing how an array can be modified in a function as it uses
// pointers.
#include <iostream>
#include <cmath>
using namespace std;

double vector_norm(double *vector, int size) {
    // Return the (Euclidian) norm of a vector;
    // Note we have added a second argument indicating the number of elements
    // in the vector.
    // Again we could list the first argument as "double vector[]" instead.
    double norm = 0.0;

    for (int i = 0; i < size; ++i) {
        norm = norm + pow(vector[i], 2);
    }
    norm = sqrt(norm);

    return norm;
}

void read_vector(double *vector, int size) {
    // Read a three component vector from stdin;

    for (int i = 0; i < size; ++i) {
        cin >> vector[i];
    }
    return;
}

int main() {
    int size = 3;
    // We can define the dimension as a variable.
    double vector[size];

    read_vector(vector, size);
    cout << vector_norm(vector, size) << endl;

    return 0;
}
