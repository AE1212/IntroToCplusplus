// Example showing arguments to main.
// Example usage:
// ./main.x 8 10.234 4.5
// ./main.x hello world
// ./main.x a ab cdef
#include <iostream>
using namespace std;

int main(int argc, char **argv) {
    // "char **argv" is a pointer to a pointer to a char. This is because
    // each char holds just 1 character, so a string of chars needs an array.
    // More than one argument can be given, so argv is a pointer to the first
    // argument, which is in turn a pointer to the first character of the
    // first argument.
    int i;

    cout << "Number of arguments: " << argc << endl;
    for (i = 0; i < argc; ++i) {
        cout << "argument " << i << " = " << argv[i] << endl;
    }
    // Note: as you will see, the first element of this array is the name
    // of the program.

    return 0;
}
