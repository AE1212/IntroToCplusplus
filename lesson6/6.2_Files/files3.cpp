// Example showing how to write to a file.
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {

    int i;

    ofstream outfile("squares.dat");
    if ( ! outfile.is_open() ) {
        cout << "Error opening file." << endl;
	return 1;
    }

    for (i = 1; i <= 10; ++i) {
        outfile << i << " " << i * i << endl;
    }

    outfile.close();

    return 0;
}
