// Examples of the use of functions.
// These allow a code to be broken down into pieces which can be reused as
// needed rather than putting everything in main.
#include <iostream>
#include <cmath>
using namespace std;

// Here we define a function of type "void". This type can be used for
// functions which do not return a value.
void loop_over_10() {
    int i;
    for (i = 0; i < 10; ++i) {
        cout << i << " " << pow(double(i), 2) << endl;
    }
}

int main() {
    // Now we can call this function where ever we like without needing to
    // rewrite the for loop code.
    loop_over_10();
    cout << "Repeating loop." << endl;
    loop_over_10();

    return 0;
}
