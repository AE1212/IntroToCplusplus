// A C++ program giving examples of how strings can be output.
#include <iostream>
using namespace std;

int main()
{
    string str1;
    string str2 = "world!";
    // The string type is defined in the standard library. If we didn't
    // specify we are using the std namespace above we would need to write
    // this as "std::string str1;".
    //
    // Variables don't strictly need to be declared at the start of the
    // function. We can assign a value to a variable as we declare as is done
    // for str2 here.

    cout << "1. Hello world!" << endl;
    // C++ uses data streams for input and output. The operator << sends
    // data to an output destination.

    cout << "2. " << "Hello " << "world!" << endl;
    // Repeated uses of << appends items to the stream.

    cout << "3. Hello ";
    cout << "world!" << endl;
    // endl is a constant marking the end of a line.

    str1 = "Hello";
    cout << "4. " << str1 << " " << str2 << endl;
    // We can use the string variables we defined earlier.

    str1 = "Hi world!";
    cout << "5. " << str1 << endl;
    // And we can redifine variables as we desire.

    cout << "Please enter a word and press enter: ";
    cin >> str1;
    // cin is defined in the standard library as standard input, and the
    // >> operator is used to read in input data.
    // You might notice if you enter text with any spaces, only the first
    // word is output. This is because "cin >>" will extract a value delimited
    // by a whitespace, which can be a space, tab or newline.
    cout << "The word you entered was: ";
    cout << str1 << endl;

    return 0;
}
