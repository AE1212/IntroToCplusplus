// A short C++ program to greet the world.

// Lines starting with // are treated as comments.

/* Multi-line comments can be created
   like this. */

#include <iostream>
/* Lines starting with # are processed first.
 * #include tells the compiler to include a header file. These contain
 * declarations of functions and variables in the associated library.
 * The angle brackets <> indicate "iostream" is part of the standard library
 * that will be provided with any C++ compiler. */

int main()
/* Here we define a function called main.
 * Every C++ program must have a main function.
 * Functions return values of a particular type, in this case we have stated
 * "main" will return an integer.
 * The parentheses are where any arguments to the function would be located. In
 * this case we say that the main function takes no arguments. */

{
    // The code associated with the function is enclosed in braces {}.

    std::cout << "Hello world!" << std::endl;
    /* This line causes the string "Hello world!" to be printed to the
     * standard output stream, followed by a newline.
     * Note the ; at the end here.
     * All C++ statements must end with a semi-colon. */

    return 0;
    /* We said the function main would return an integer, and we do so here.
     * The return statement causes the function to end and the associated
     * value to be passed back to whatever called the function.
     * 0 is generally used to mean the code has completed successfully.
     * Other values are typically used to indicate various types of errors
     * have occurred. */
}
