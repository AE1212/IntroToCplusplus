// C++ doesn't take whitespace or newlines into account. The following code
// is perfectly valid, but much more difficult to read.
// It's considered good practice to have the layout and indentation reflect
// the logical structure of the code, and it will make your code much easier
// to work with.
// Establishing good habits now will save you a lot of time in the future.
#include<iostream>
using namespace std;int main(){cout<<"Hello world!"<<endl;return 0;}
