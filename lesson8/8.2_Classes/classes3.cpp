// Example showing how to set private variables in a class.
#include <iostream>
#include <string>

using namespace std;

class Atom {
    // These variables are all now private. Here we're also using a naming
    // convention where private variables end in '_'.
    int atomic_number_;
    double mass_number_;
    double radius_pm_;
    string symbol_;

    public:
        // This function is able to output the private variables in the class
        // since it is also contained in the class.
        void output_info() {
            cout << symbol_ << endl;
            cout << "Atomic number: " << atomic_number_ << endl;
            cout << "Mass number: " << mass_number_ << endl;
            cout << "Atomic radius (pm): " << radius_pm_ << endl;
        }

        // Instead of defining the full function, we can define the interface
        // and define the function later.
        void set_info(string, int, double, double);
} ;

// Note the syntax here to define the function contained in a class.
void Atom::set_info(string s, int a, double m, double r) {

    symbol_ = s;
    atomic_number_ = a;
    mass_number_ = m;
    radius_pm_ = r;
}

int main() {
    // Initialize two objects of the Atom class.
    Atom hydrogen, lithium;

    hydrogen.set_info("H", 1, 1.008, 25);
    lithium.set_info("Li", 3, 6.941, 145);

    hydrogen.output_info();
    cout << endl;
    lithium.output_info();

    return 0;
}
