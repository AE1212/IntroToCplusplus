// Example showing how to use classes.
#include <iostream>
#include <string>

using namespace std;

// We use the following construction to create a class named
// Atom, containing several different types of variables, all marked
// as public.
class Atom {
    public:
        int atomic_number;
        double mass_number;
        double radius_pm;
        string symbol;
} ;


// This function will output the information stored in the structure.
// variables contained in the structure are accessed using the
// structure_name.variable_name construction.
void output_atom_info(Atom a) {
    cout << a.symbol << endl;
    cout << "Atomic number: " << a.atomic_number << endl;
    cout << "Mass number: " << a.mass_number << endl;
    cout << "Atomic radius (pm): " << a.radius_pm << endl;
}

int main() {
    // Initialize two objects of the Atom class.
    Atom hydrogen, lithium;

    // Since the class variables are public we can set them directly.
    hydrogen.atomic_number = 1;
    hydrogen.mass_number = 1.008;
    hydrogen.symbol = "H";
    hydrogen.radius_pm =  25;

    lithium.atomic_number = 3;
    lithium.mass_number = 6.941;
    lithium.symbol = "Li";
    lithium.radius_pm = 145;

    output_atom_info(hydrogen);
    cout << endl;
    output_atom_info(lithium);

    return 0;
}
