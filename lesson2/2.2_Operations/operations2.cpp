// Examples of mathematical operations and functions using cmath.
// Note the extra #include statement here. This makes additional mathematical
// operations available.
#include <cmath>
#include <iostream>
using namespace std;

int main()
{
    cout << "pow(2.4, 2) = " << pow(2.4, 2) << endl; // 2.4^2
    cout << "pow(2.4, 2.4) = " << pow(2.4, 2.4) << endl; // 2.4^2.4
    cout << "exp(2.4) = " << exp(2.4) << endl; // e^2.4
    cout << "log(2.4) = " << log(2.4) << endl; // log(2.4) (natural)
    cout << "sqrt(2.4) = " << sqrt(2.4) << endl; // 2.4^0.5
    cout << "log10(2.4) = " << log10(2.4) << endl; // log10(2.4) (base 10)
    cout << "sin(3.14) = " << sin(3.14) << endl; // NB: Argument in radians.
    // The other trigonmentic functions are available in the same way.
    cout << "abs(-11.4) = " << abs(-11.4) << endl;
    // Many other mathematical functions are available.

    // Care needs to be taken as these functions may not be defined for
    // integer arguments (apart from exponention where integer powers are
    // allowed). It will depend on the C++ standard used by your compiler:
    // the most recent (C++11) will convert the argument to a double if given
    // an integer.
    // Generally, if the function is unambiguous then an integer argument will
    // be implicitly converted to a double.

    // Some compilers will give an error if asked to compile the following:
    // cout << "pow(2, 2) = " << pow(2, 2) << endl;
    //
    // Instead we can explicity convert the first argument to a double.
    // This can be done in one of two equivalent ways.
    cout << "pow( double(2), 2) = " << pow( double(2), 2) << endl;
    cout << "pow( (double) 2, 2) = " << pow( (double) 2, 2) << endl;

    // In general one can do either of:
    // new_type (variable);
    // (new_type) variable;

    return 0;
}
