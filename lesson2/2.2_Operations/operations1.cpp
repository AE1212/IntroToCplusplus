// Examples of basic mathematical operations.
#include <iostream>
using namespace std;

int main()
{
    int i1, i2, i3;
    // We can initialize several variables of one type separated by commas.

    // +, - and * work as expected.
    cout << "2 + 2 = " << 2 + 2 << endl;
    cout << "1.0 + 9.1 = " << 1.0 + 9.1 << endl;
    cout << "10 - 5 = " << 10 - 5 << endl;
    cout << "1.4 * 1.5 = " << 1.4 * 1.5 << endl;

    // Take care of the type when dividing. Integer division is truncated.
    cout << "3.0 / 4.0 = " << 3.0 / 4.0 << endl;
    cout << "3 / 4. = " << 3 / 4. << endl;
    cout << "3. / 4 = " << 3. / 4 << endl;
    cout << "3 / 4 = " << 3 / 4 << endl;
    cout << "-3 / 4 = " << -3 / 4 << endl;

    // Modulus is represented by %
    cout << "5 % 2 =  " << 5 % 2 << endl;
    // Take care though, as this is really a remainder function rather
    // than the usual mathematical definition.
    cout << "-5 % 2 = " << -5 % 2 << endl;

    i1 = 1;
    i2 = 3;
    // The result of any operation can be stored in a variable.
    i3 = 10 / i2; // = 3
    cout << i3 << endl;
    // Variables can appear both on the left and right of the =.
    i3 = i3 * i2; // = 9
    cout << i3 << endl;

    // *, / and % have a higher precedence than + and -.
    i1 = i3 * 2 + 2 * 0; // = 18
    cout << i1 << endl;

    // Operations of the same precedence are carried out left to right.
    i2 = 7 / 3 * 2 * 3 / 4 % 2; // = 1
    cout << i2 << endl;

    // Compound assignment operators can also be used.
    i1 += 5; // i1 = i1 + 5;
    i2 -= 4; // i2 = i2 - 4;
    i3 *= 2; // i3 = i3 * 2;
    i1 /= 2; // i1 = i1 / 2;
    i2 %= 3; // i2 = i2 % 2;

    // Additionally increment (++) and decrement (--) operators are common.
    ++i1; // i1 = i1 + 1;
    --i2; // i2 = i2 - 1;
    // Note you can also use i1++ and i2--; most of the time they can be used
    // interchangeably, but they do mean slightly different things.
    // Generally it's safest to stick to the ++i1 form.

    return 0;
}
