// Examples of types and variables.
#include <iostream>
using namespace std;

int main()
{
    bool s;
    char c;
    short a;
    unsigned short b;
    int i;
    unsigned int j;
    float f;
    double d;

    string str;
    // The string type is defined in the std library. Without using the
    // std namespace above we would have to write "std::string str;"

    // bool (1 byte)
    s = false;
    cout << "s (bool, false): " << s << endl;
    s = true;
    cout << "s (bool, true): " << s << endl;
    s = 0;
    cout << "s (bool, 0): " << s << endl;
    s = 1;
    cout << "s (bool, 1): " << s << endl;
    s = 2;
    cout << "s (bool, 2): " << s << endl;
    s = 3;
    cout << "s (bool, 3): " << s << endl;

    // char (1 byte)
    c = 'a'; // Note you need to use single quotes to assign a character.
    cout << "c (char, 'a'): " << c << endl;
    c = 101;
    cout << "c (char, 101): " << c << endl;
    c = 357;
    cout << "c (char, 357): " << c << endl;

    // short (2 bytes)
    a = -5210;
    cout << "a (short, -5210): " << a << endl;
    a = 32767;
    cout << "a (short, 32767): " << a << endl;
    a = 32768;
    cout << "a (short, 32768): " << a << endl;
    b = 32768;
    cout << "b (unsigned short, 32768): " << b << endl;
    b = -100;
    cout << "b (unsigned short, -100): " << b << endl;
    b = 65535;
    cout << "b (unsigned short, 65535): " << b << endl;
    b = 65536;
    cout << "b (unsigned short, 65536): " << b << endl;

    // int (4 bytes)
    i = -2147483648;
    cout << "i (int, -2147483648): " << i << endl;
    i = -2147483649;
    cout << "i (int, -2147483649): " << i << endl;
    j = 4294967295;
    cout << "j (unsigned int, 4294967295): " << j << endl;
    j = 4294967296;
    cout << "j (unsigned int, 4294967296): " << j << endl;

    // float (4 bytes)
    f = 1.0;
    cout << "f (float, 1.0): " << f << endl;
    f = 3.4e30;
    cout << "f (float, 3.4e30): " << f << endl;
    f = 3.4e40;
    cout << "f (float, 3.4e40): " << f << endl;

    // double (8 bytes)
    d = 1.0;
    cout << "d (double, 1.0): " << d << endl;
    d = 3.4e30;
    cout << "d (double, 3.4e30): " << d << endl;
    d = 3.4e300;
    cout << "d (double, 3.4e300): " << d << endl;
    d = 3.4e310;
    cout << "d (double, 3.4e310): " << d << endl;

    // string
    str = "Hello.";
    cout << str << endl;

    // One can read in a value to one of these variable types as before
    cout << "Please enter an integer: " << endl;
    cin >> i;
    cout << "The number you entered was " << i << endl;

    return 0;
}
